# uniapp-gaode-demo

#### 介绍
使用高德地图、uniapp写的地图小程序，功能完善中

使用到的高德地图微信小程序插件参考：https://lbs.amap.com/api/wx/reference/core，可查看每个方法的success参数说明
uni-app Dcloud关于位置的说明：[获取位置，查看位置，位置更新，地图组件控制](https://uniapp.dcloud.net.cn/api/location/location.html)

CSDN文章：[https://blog.csdn.net/csdnyp/article/details/127513575](https://blog.csdn.net/csdnyp/article/details/127513575)