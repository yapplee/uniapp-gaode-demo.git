import Vue from "vue";
import Vuex from 'vuex';

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {

		origin: '', //出发地
		originLng: '', //经度
		originLat: '', //纬度
		destination: '', //目的地
		destinationLng: '', //经度
		destinationLat: '', //纬度

	},
	mutations: {
		setOrigin(state, data) {
			this.state.origin = data
		},
		setOriginLng(state, data) {
			this.state.originLng = data
		},
		setOriginLat(state, data) {
			this.state.originLat = data
		},
		setDestination(state, data) {
			this.state.destination = data
		},
		setDestinationLng(state, data) {
			this.state.destinationLng = data
		},
		setDestinationLat(state, data) {
			this.state.destinationLat = data
		},

	},
	actions: {},
	modules: {},
	getters: {
		getOrigin: (data) => {
			return data.origin;
		},
		getOriginLng: (data) => {
			return data.originLng;
		},
		getOriginLat: (data) => {
			return data.originLat;
		},
		getDestination: (data) => {
			return data.destination;
		},
		getDestinationLng: (data) => {
			return data.destinationLng;
		},
		getDestinationLat: (data) => {
			return data.destinationLat;
		},

	}
})
export default store
