import {axios} from './http'

// get 请求
const get = function (url, params) {
    return new Promise((resolve, reject) => {
        axios.get(url, {
            params: params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

// POST 请求
const post = function (url, data) {
    return new Promise((resolve, reject) => {
        axios.post(url, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

// PUT 请求
const put = function (url, data) {
    return new Promise((resolve, reject) => {
        axios.put(url, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

// DELETE 请求
const del = function (url, data) {
    return new Promise((resolve, reject) => {
        axios.delete(url, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}


//上传文件
const postUpload = function (url, data, params) {
    return new Promise((resolve, reject) => {
        axios({
            url: url,
            method: 'POST',
            headers: {
                'Content-type': 'multipart/form-data;boundary=${new Date().getTime()}',
            },
            data: data,
            params: params
        }).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err)
        })
    })
}

export default {get, post, put, del, postUpload}
