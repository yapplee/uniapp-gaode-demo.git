import axios from 'axios'

 axios.defaults.baseURL = 'http://192.168.20.12:8888'
//axios.defaults.baseURL = 'http://172.16.28.4:8888'
axios.defaults.timeout = 30000; // 超时时间

// request拦截器,在请求之前做一些处理
axios.interceptors.request.use(
	config => {
		// if (store.state.token) {
		//     // 给请求头添加user-token
		//     config.headers["user-token"] = store.state.token;
		// }
		config.headers.common['Authorization'] =uni.getStorageSync('token')
		return config;
	},
	error => {
		console.log(error); // for debug
		return Promise.reject(error);
	}
);

//配置成功后的拦截器
axios.interceptors.response.use(res => {
	return res
}, error => {
	return Promise.reject(error)
})

//uniapp适配axios，否则会报错：adapter is not a function
axios.defaults.adapter = function(config) {
  return new Promise((resolve, reject) => {
      //console.log(config)
      var settle = require('axios/lib/core/settle');
      var buildURL = require('axios/lib/helpers/buildURL');
      uni.request({
          method: config.method.toUpperCase(),
          url: config.baseURL + buildURL(config.url, config.params, config.paramsSerializer),
          header: config.headers,
          data: config.data,
          dataType: config.dataType,
          responseType: config.responseType,
          sslVerify: config.sslVerify,
          complete: function complete(response) {
              response = {
                  data: response.data,
                  status: response.statusCode,
                  errMsg: response.errMsg,
                  header: response.header,
                  config: config
              };

              settle(resolve, reject, response);
          }
      })
  })
}


export {
	axios
}
